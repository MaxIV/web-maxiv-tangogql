#!/usr/bin/env python3

"""Tango GraphQL package."""

__all__ = ["run_tangogql"]

from tangogql.aioserver import run as run_tangogql
