import PyTango


class ErrorParser:
    @staticmethod
    def remove_duplicated_errors(errors):
        seen = set()
        result_set = []
        for message in errors:
            if message["reason"] != "None":
                if isinstance(message, dict):
                    t = tuple(sorted(message.items()))
                else:
                    t = str(message)
                if t not in seen:
                    seen.add(t)
                    result_set.append(message)
        return result_set

    @staticmethod
    def parse(error):
        message = {}
        if hasattr(error, "original_error"):
            if isinstance(error.original_error, tuple):
                error_original_error = error.original_error[0]
            else:
                error_original_error = error.original_error

            if isinstance(
                error_original_error,
                (
                    PyTango.ConnectionFailed,
                    PyTango.CommunicationFailed,
                    PyTango.DevFailed,
                ),
            ):
                for e in error_original_error.args:
                    if e.reason == "" or e.reason == "API_CorbaException":
                        pass
                    elif e.reason in [
                        "API_CantConnectToDevice",
                        "API_DeviceTimedOut",
                    ]:
                        message["device"] = e.desc.split("\n")[0].split(" ")[
                            -1
                        ]
                        message["desc"] = e.desc.split("\n")[0]
                        message["reason"] = e.reason.split("_")[-1]
                    elif e.reason == "API_AttributeFailed":
                        [device, attribute] = e.desc.split(",")
                        message["device"] = device.split(" ")[-1]
                        message["attribute"] = attribute.split(" ")[-1]
                    elif e.reason == "API_AttrValueNotSet":
                        message["reason"] = e.reason.split("_")[-1]
                        message["field"] = e.desc.split(" ")[1]
                    else:
                        message["reason"] = e.reason
                        message["desc"] = e.desc
            else:
                message["reason"] = str(error)
        else:
            message["reason"] = str(error)
        return message
