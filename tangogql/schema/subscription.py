"""Module containing the Subscription implementation."""

import logging as logger
import traceback

from graphene import Field, Float, List, ObjectType, String

from tangogql.schema.base import subscriptions as subs
from tangogql.schema.types import ScalarTypes


class AttributeFrame(ObjectType):
    attribute = String()
    device = String()
    full_name = String()
    value = ScalarTypes()
    write_value = ScalarTypes()
    quality = String()
    timestamp = Float()

    def resolve_full_name(self, info):
        return f"{self.device}/{self.attribute}"


SLEEP_DURATION = 3.0


class Subscription(ObjectType):
    """Subscription to an object"""

    attributes = Field(AttributeFrame, full_names=List(String, required=True))

    async def resolve_attributes(self, info, full_names):
        """Setup attribute subscription and return an async gen"""

        async with subs.attribute_reads(full_names) as attribute_reads:
            async for device, read in attribute_reads:
                try:
                    name = read.name.lower()
                    sec = read.time.tv_sec
                    micro = read.time.tv_usec
                    timestamp = sec + micro * 1e-6
                    value = getattr(read, "value", None)
                    write_value = getattr(read, "w_value", None)
                    quality = read.quality.name
                    logger.debug(
                        f"FRAME: Device: {device} Attribute: {name} "
                        f"Timestamp: {timestamp}"
                    )
                    yield AttributeFrame(
                        device=device,
                        attribute=name,
                        value=value,
                        write_value=write_value,
                        quality=quality,
                        timestamp=timestamp,
                    )
                except Exception as e:
                    logger.error(
                        f"Exception when resolving attributes: {str(e)} "
                    )
                    traceback.print_exc()
                    raise e
