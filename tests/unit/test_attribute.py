import asyncio
import logging as logger
import unittest.mock as mock

import pytest
import tango

import tangogql.aioattribute.attribute
from tangogql.aioattribute.attribute import Attribute

# prepare a few Tango naming attributes. These are the values and combinations
# that we expect the Attribute implementation to return when identifying an
# updated attribute value.
DEVICE_FQDN = "domain/family/member"
FQDN_DOMAIN, FQDN_FAMILY, FQDN_MEMBER = DEVICE_FQDN.split("/")
FQDN_DOMAIN_FAMILY = "/".join([FQDN_DOMAIN, FQDN_FAMILY])


@pytest.fixture(autouse=True)
def run_around_tests():
    """
    Create a new event loop for every test. This stops scheduled jobs from
    previous tasks from leaking into the subsequent test.
    """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    yield
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()


def run_until_complete(coro):
    """
    Run a coroutine until complete.

    :param coro: the coroutine to run
    :return:
    """
    return asyncio.get_event_loop().run_until_complete(coro)


def step_event_loop(ticks=1, loop=None):
    """
    Step the asyncio event loop by the given number of ticks.

    :param ticks: ticks to iterate (default=1)
    :param loop: event loop to cycle (default=asyncio event loop)
    """
    if loop is None:
        loop = asyncio.get_event_loop()

    for tick in range(ticks):
        loop.call_soon(loop.stop)
        loop.run_forever()


@pytest.fixture(autouse=True)
def patch_asyncio_sleep(monkeypatch):
    """
    Patch asyncio sleep with an version that has zero delay, regardless of the
    requested sleep duration.

    This code is a small adaptation of the standard sleep implementation in
    the asyncio.tasks module.

    :param monkeypatch: pytest monkeypatch module
    """

    async def sleep(delay, result=None, *, loop=None):
        """Coroutine that completes after a given time (in seconds)."""
        logger.debug(f"waiting for  a delay of {delay} and a loop of {loop}")
        await asyncio.sleep(0)
        return result

    monkeypatch.setattr(asyncio, "sleep", sleep)


@pytest.fixture
def device_proxy(monkeypatch):
    """
    Patch the Tango DeviceProxy class with a mock object for the duration of
    a unit test.

    This function provides a pytest test fixture that behaves as a drop-in
    replacement for a Pytango device proxy instance. Device proxy methods that
    are awaited are replaced with no-op coroutine mocks. If left unaltered,
    this results in an Attribute subscription to change events and a null
    polling return value. The coroutine mocks should be replaced if alternate
    behaviour is required.

    :param monkeypatch: pytest monkeypatch module
    :return:
    """
    # This MagicMock becomes the DeviceProxy instance used by the Attribute
    # code under test.
    mock_device_proxy = mock.MagicMock()

    # The DeviceProxy.subscribe_event() method is awaited, so replace the
    # default non-awaitable MagicMock with an awaitable CoroutineMock
    # instance.
    #
    # Leaving this mock unchanged will result in a subscription to change
    # events. Tests that require different behaviour should replace this
    # mock.
    mock_device_proxy.subscribe_event = mock.AsyncMock(return_value="12345")

    # The DeviceProxy.unsubscribe_event() method is awaited, so replace the
    # default non-awaitable MagicMock with an awaitable CoroutineMock
    # instance.
    mock_device_proxy.unsubscribe_event = mock.AsyncMock(return_value="12345")

    # The DeviceProxy.read_attribute() method is awaited when the polling
    # mechanism is used. Replace the default non-awaitable MagicMock with an
    # awaitable CoroutineMock instance.
    #
    # Note that this implementation returns None each time the attribute value
    # is read. Tests that require different behaviour should replace this
    # mock.
    mock_device_proxy.read_attribute = mock.AsyncMock(None)

    def device_proxy_init(*args, **kwargs):
        """
        Fake DeviceProxy constructor that returns a fixed MagicMock instance.

        :param args: DeviceProxy args (unused).
        :param kwargs: DeviceProxy kwargs (unused).
        :return:
        """
        logger.debug(
            f"Mock device proxy called with args =\
                {args} and  kwargs = {kwargs}"
        )
        return mock_device_proxy

    # Patch the imported DeviceProxy class with our function that returns the
    # prepared MagicMock instance...
    monkeypatch.setattr(
        tangogql.aioattribute.attribute, "DeviceProxy", device_proxy_init
    )
    # ... and pass the pre-prepared MagicMock fixture to the tests
    return mock_device_proxy


@mock.patch("tangogql.aioattribute.attribute.DeviceProxy")
def test_device_proxy_constructed_correctly(MockDeviceProxy):
    """
    Verify that the DeviceProxy created by the Attribute connects to the
    expected device.

    :param MockDeviceProxy: mock DeviceProxy class
    """
    # create an Attribute
    _ = Attribute(DEVICE_FQDN)

    # asserts that DeviceProxy constructor is called with expected device name
    # and using asyncio green mode
    MockDeviceProxy.assert_called_with(
        FQDN_DOMAIN_FAMILY, green_mode=tango.GreenMode.Asyncio
    )


def test_that_when_sub_is_switched_off_the_attr_poll_is_not_attempted(
    device_proxy,
):
    """
    Registering an Attribute listener should result, in the first instance, in
    a subscription to Tango change events.

    :param device_proxy: mock device proxy test fixture
    """
    # create an Attribute and attach a listener.
    attr = Attribute(DEVICE_FQDN, use_evt=False, polling_interval=3)
    mock_listener = mock.MagicMock()
    run_until_complete(attr.add_listener(mock_listener))

    # The PyTango DeviceProxy should not have been subscribed to
    device_proxy.subscribe_event.assert_not_awaited()

    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))

    # With events disabled we poll via the read_attribute method.
    expected_value = tango.DeviceAttribute()
    expected_value.value = "12345"
    device_proxy.read_attribute = mock.AsyncMock(return_value=expected_value)


def test_subscribe_to_change_events_first(device_proxy):
    """
    Registering an Attribute listener should result, in the first instance, in
    a subscription to Tango change events.

    :param device_proxy: mock device proxy test fixture
    """
    # create an Attribute and attach a listener
    # where  we expect to be using tango polling
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    mock_listener = mock.MagicMock()
    run_until_complete(attr.add_listener(mock_listener))

    # The PyTango DeviceProxy should have received exactly one call for a
    # subscription to change events. The _on_event method should be called
    # on event reception.
    device_proxy.subscribe_event.assert_awaited_once_with(
        FQDN_MEMBER,
        tango.EventType.CHANGE_EVENT,
        attr._on_event,
        extract_as=tango.ExtractAs.List,
        green_mode=tango.GreenMode.Asyncio,
    )


def test_subscribe_to_periodic_events_if_change_events_not_available(
    device_proxy,
):
    """
    If change events are unavailable, registering an Attribute listener should
    cause a fall-back subscription to periodic events.

    :param device_proxy: mock device proxy test fixture
    """
    # Replace the default subscribe_event mock with a version that should
    # cause a subscription to periodic events. The first subscription attempt,
    # the one that subscribes to CHANGE_EVENTs, raises a DevFailed exception.
    # The subsequent subscription to PERIODIC_EVENTs should succeed.
    device_proxy.subscribe_event = mock.AsyncMock(
        side_effect=[tango.DevFailed, None]
    )

    # create an Attribute and attach a listener where
    # we expect to be using tango polling
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    mock_listener = mock.MagicMock()
    run_until_complete(attr.add_listener(mock_listener))

    # confirm that two event subscription attempts were made. The second,
    # successful, subscription attemps should be for periodic events.
    assert device_proxy.subscribe_event.await_count == 2
    device_proxy.subscribe_event.assert_awaited_with(
        FQDN_MEMBER,
        tango.EventType.PERIODIC_EVENT,
        attr._on_event,
        extract_as=tango.ExtractAs.List,
        green_mode=tango.GreenMode.Asyncio,
    )


def test_subscribe_falls_back_to_polling_when_events_unavailable(device_proxy):
    """
    If events are unavailable, registering an Attribute listener should cause
    polling of the attribute to begin.

    :param device_proxy: mock device proxy test fixture
    """
    # Replace the default subscribe_event mock with a version that rejects all
    # subscription attempts.
    device_proxy.subscribe_event = mock.AsyncMock(side_effect=tango.DevFailed)

    # With events disabled we fall back to polling via the read_attribute
    # method. Like subscribe_event above, this method is awaited so it must be
    # replaced with an awaitable CoroutineMock, which we set to return a set
    # value.
    expected_value = tango.DeviceAttribute()
    expected_value.value = "12345"
    device_proxy.read_attribute = mock.AsyncMock(return_value=expected_value)

    # create an Attribute and attach a listener where
    # we expect to be using tango polling
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))

    # The attribute should have been polled...
    device_proxy.read_attribute.assert_called_once_with(
        FQDN_MEMBER, extract_as=tango.ExtractAs.List
    )
    # ... and whenever the attribute is polled, the listener should be notified
    listener.put_nowait.assert_called_once_with(
        (FQDN_DOMAIN_FAMILY, expected_value)
    )


def test_listener_notified_on_event_reception(device_proxy):
    """
    Verify that Attribute listeners are notified when an event is received.

    :param device_proxy: mock device proxy test fixture
    """
    # Create an Attribute and register a listener
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))

    # Create a mock Tango event and publish it. PyTango would do this by
    # calling the Attribute._on_event function that was supplied during event
    # subscription.
    event = mock.MagicMock(spec=tango.EventData)
    expected_value = "12345"
    event.attr_value = expected_value
    attr._on_event(event)

    # confirm that the listener was notified of the event
    listener.put_nowait.assert_called_once_with(
        (FQDN_DOMAIN_FAMILY, expected_value)
    )


def test_listener_still_notified_on_event_error(device_proxy):
    """
    Verify that listeners are notified on event errors.
    """
    # Create an Attribute and register a listener
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))

    # Create and publish an event in an error state
    event = mock.MagicMock(spec_set=tango.EventData)
    expected_value = "12345"
    event.err = True
    event.attr_value = expected_value
    attr._on_event(event)

    # confirm that the listener was still notified of the event
    listener.put_nowait.assert_called_once_with(
        (FQDN_DOMAIN_FAMILY, expected_value)
    )


def test_unsubscribed_listener_not_notified_on_event_reception(device_proxy):
    """
    Once unsubscribed, Attribute listeners should not be notified when events
    are received.

    In practice, the _on_event method would not be published once
    unsubscription is complete, so this is more a check that the listener has
    been removed from Attribute's internal list of listeners.

    :param device_proxy: mock device proxy test fixture
    """
    # Create an Attribute, register a listener, then unregister the listener
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))
    run_until_complete(attr.remove_listener(listener))

    # Create an event and publish it
    event = mock.MagicMock(spec=tango.EventData)
    attr._on_event(event)

    # confirm that the listener was not notified of the event
    listener.put_nowait.assert_not_called()


def test_unsubscribed_listener_not_notified_on_polling(device_proxy):
    """
    Once unsubscribed, Attribute listeners should not be notified when the
    device attribute is polled.

    :param device_proxy: mock device proxy test fixture
    """
    # Replace the default subscribe_event mock with a version that rejects all
    # subscription attempts, thus forcing polling
    device_proxy.subscribe_event = mock.AsyncMock(side_effect=tango.DevFailed)

    # Create an Attribute and register a listener.
    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    run_until_complete(attr.add_listener(listener))

    # zero the number of calls made to listener.put_nowait() and remove the
    # listener. No further calls to listener.put_nowait() should be made.
    run_until_complete(attr.remove_listener(listener))
    listener.put_nowait.reset_mock()

    # run the event loop sufficiently long for the polling to occur a few times
    step_event_loop(ticks=10)

    # confirm that the listener was not notified of the event
    listener.put_nowait.assert_not_called()


def test_polling_loop_stops_when_all_listeners_unsubscribe(device_proxy):
    """
    Verify that the polling loop stops when all listeners have unsubscribed.

    :param device_proxy: mock device proxy test fixture
    """
    # Replace the default subscribe_event mock with a version that rejects all
    # subscription attempts, thus forcing polling
    device_proxy.subscribe_event = mock.AsyncMock(side_effect=tango.DevFailed)

    attr = Attribute(DEVICE_FQDN, use_evt=True)
    listener = mock.MagicMock()
    try:
        run_until_complete(attr.add_listener(listener))
    except asyncio.CancelledError:
        print("Tasks has been canceled")

    # zero the number of calls made to listener.put_nowait() and the device
    # proxy, then remove the listener. No further calls to listener or device
    # proxy should be made.
    run_until_complete(attr.remove_listener(listener))
    device_proxy.read_attribute.reset_mock()
    listener.put_nowait.reset_mock()

    # run the event loop sufficiently long for values to have been polled
    # should a listener have remained subscribed
    step_event_loop(10)

    # Assert the above
    device_proxy.read_attribute.assert_not_called()
    device_proxy.read_attribute.assert_not_awaited()
    listener.put_nowait.assert_not_called()


# noinspection PyBroadException
