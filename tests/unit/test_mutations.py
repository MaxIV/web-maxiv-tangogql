import sys
from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from tests.unit.test_schema_mocks import MockDB, MockDeviceProxyCache

mock_base = MagicMock()
sys.modules["tangogql.schema.base"] = mock_base

with patch.dict("sys.modules", {"tangogql.schema.base": mock_base}):
    from tangogql.schema.mutations import (
        DeleteDeviceProperty,
        ExecuteDeviceCommand,
        PutDeviceProperty,
        SetAttributeValue,
        get_username_from_info,
    )


mock_base = MagicMock()
sys.modules["tangogql.schema.base"] = mock_base
mock_base.db = MockDB()
mock_base.proxies = MockDeviceProxyCache()


@pytest.fixture
def info():
    """Mock authentification infos"""
    fake_info = type("info", (), {})()
    config = type("config", (), {})()
    client = type("client", (), {})()
    config.required_groups = ["group"]
    client.user = "fred"
    client.groups = ["group"]
    fake_info.context = {"config": config, "client": client}
    yield fake_info


@pytest.fixture
def device_test_attributes(info):
    """deviceData fixture used by unit tests to send queries
    to the TangoGQL code simulating requests from WebJive"""
    device_attributes = {
        "argin": "",
        "info": info,
        "command": "Status",
        "device": "sys/database/2",
        "name": "RandomAttribute",
        "value": 20,
    }
    yield device_attributes


def test_can_extract_name_from_info_block(device_test_attributes):
    """
    basic test of extracting the name from the
    information block provided - this is used
    inside all of the mutate functions as we only
    currently use the info to get the user name
    """
    name = "fred"
    assert name == get_username_from_info(device_test_attributes["info"])


@pytest.mark.asyncio
async def test_basic_execute_device_command_mutate(device_test_attributes):
    mock_response = MagicMock()
    mock_response.message = ["Success"]
    mock_response.output = "command_output"
    mock_response.ok = True
    async_mock = AsyncMock(return_value=mock_response)
    with patch.object(ExecuteDeviceCommand, "mutate", async_mock):
        result = await ExecuteDeviceCommand().mutate(
            device_test_attributes["info"],
            device_test_attributes["device"],
            device_test_attributes["command"],
            device_test_attributes["argin"],
        )
        assert result.message[0] == "Success"


@pytest.mark.asyncio
async def test_basic_set_device_attribute_value_mutate(device_test_attributes):
    mock_response = MagicMock()
    mock_response.message = ["Success"]
    mock_response.ok = True
    async_mock = AsyncMock(return_value=mock_response)
    with patch.object(SetAttributeValue, "mutate", async_mock):
        result = await SetAttributeValue().mutate(
            device_test_attributes["info"],
            device_test_attributes["device"],
            device_test_attributes["name"],
            device_test_attributes["value"],
        )
        assert result.message[0] == "Success"


def test_basic_put_device_property_mutate_works(device_test_attributes):
    mock_response = MagicMock()
    mock_response.message = ["Success"]
    mock_response.ok = True
    with patch.object(PutDeviceProperty, "mutate", return_value=mock_response):
        result = PutDeviceProperty().mutate(
            device_test_attributes["info"],
            device_test_attributes["device"],
            device_test_attributes["name"],
            device_test_attributes["value"],
        )
        assert result.message[0] == "Success"


def test_basic_delete_device_property_mutate_works(device_test_attributes):
    mock_response = MagicMock()
    mock_response.message = ["Success"]
    mock_response.ok = True
    with patch.object(
        DeleteDeviceProperty, "mutate", return_value=mock_response
    ):
        result = DeleteDeviceProperty().mutate(
            device_test_attributes["info"],
            device_test_attributes["device"],
            device_test_attributes["name"],
        )
        assert result.message[0] == "Success"
