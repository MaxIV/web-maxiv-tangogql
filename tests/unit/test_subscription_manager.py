import asyncio
from unittest.mock import AsyncMock, patch

import pytest

from tangogql.aioattribute.manager import SubscriptionManager


@pytest.fixture(scope="session")
def event_loop():
    """
    Create an event loop for the session.
    This stops scheduled jobs from previous
    tasks from leaking into the subsequent test.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def subscription_manager(event_loop):
    asyncio.set_event_loop(event_loop)
    return SubscriptionManager(use_evt=False, polling_interval=1)


@patch("tangogql.aioattribute.manager.Attribute")
def test_get_attribute_creates_new(MockAttribute, subscription_manager):
    assert len(subscription_manager.attributes) == 0
    attribute = subscription_manager._get_attribute("test_attribute")
    assert len(subscription_manager.attributes) == 1
    assert subscription_manager.attributes["test_attribute"] == attribute


@patch("tangogql.aioattribute.manager.Attribute")
def test_get_attribute_returns_existing(MockAttribute, subscription_manager):
    attribute1 = subscription_manager._get_attribute("test_attribute")
    attribute2 = subscription_manager._get_attribute("test_attribute")
    assert attribute1 == attribute2
    assert len(subscription_manager.attributes) == 1


@pytest.mark.asyncio
@patch("tangogql.aioattribute.manager.Attribute")
@patch("tangogql.aioattribute.manager.asyncio.Queue")
async def test_attribute_reads_context_manager(
    MockQueue, MockAttribute, subscription_manager
):
    attribute = subscription_manager._get_attribute("test_attribute")
    attribute.add_listener = AsyncMock()
    attribute.remove_listener = AsyncMock()

    async with subscription_manager.attribute_reads(["test_attribute"]):
        assert attribute.add_listener.called
        assert not attribute.remove_listener.called

    assert attribute.remove_listener.called


@pytest.mark.asyncio
@patch("tangogql.aioattribute.manager.Attribute")
@patch("tangogql.aioattribute.manager.asyncio.Queue")
async def test_attribute_reads_yields_values(
    MockQueue, MockAttribute, subscription_manager
):
    attribute = subscription_manager._get_attribute("test_attribute")
    listener = MockQueue.return_value
    listener.get = AsyncMock(return_value="test_value")
    attribute.add_listener = AsyncMock()
    attribute.remove_listener = AsyncMock()

    async with subscription_manager.attribute_reads(["test_attribute"]) as gen:
        async for value in gen:
            assert value == "test_value"
            break


@pytest.mark.asyncio
@patch("tangogql.aioattribute.manager.Attribute")
@patch("tangogql.aioattribute.manager.asyncio.Lock")
async def test_concurrent_subscriptions(
    MockLock, MockAttribute, subscription_manager
):
    attribute1 = subscription_manager._get_attribute("test_attribute1")
    attribute2 = subscription_manager._get_attribute("test_attribute2")
    attribute1.add_listener = AsyncMock()
    attribute2.add_listener = AsyncMock()
    attribute1.remove_listener = AsyncMock()
    attribute2.remove_listener = AsyncMock()

    async with subscription_manager.attribute_reads(
        ["test_attribute1", "test_attribute2"]
    ):
        assert attribute1.add_listener.called
        assert attribute2.add_listener.called


if __name__ == "__main__":
    pytest.main()
